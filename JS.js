$(document).ready(function() {
    $("#but").click(function() {
        var text = $("#text");
        var but = $("#but");
        var backbut = but.html("push back");
        var div = $("div");
        var hasP = $("#first").has("p");
        var hasP2 = $("#second").has("p");
        var first =  $("#first");
        var second =  $("#second");
        console.log(hasP.length);
        if (hasP.length) {
            text.appendTo("#second");
            but.html("push back");

        } else {
            text.appendTo("#first");
            but.html("push");
        }
        if (hasP2.length) {
            first.css("background", "none");
            second.css("background","linear-gradient(to left, #0f7000, #93de85)")
        } else {
            second.css("background", "none");
            first.css("background","linear-gradient(to top, #fefcea, #f1da36)")
        }
    });

});